all:
	echo "CHECKING FOR REQUIRED PACKAGES"
	dpkg -l | grep cm-super || exit 1
	pdflatex schools.tex

clean:
	rm -f *.aux *.bcf *.xml *.toc *.out
	rm -f *.lot *.lof *.blg *.bbl *.log
	rm -f *.pdf
